const express = require('express');
const router = express.Router();

const {getTrucks, getTruck, createTruck, editTruck, deleteTruck, assignTruck} = require('../controllers/truckController');
const {ifDriverFree} = require('../middlewares/checkIfDriverFree');
const authMiddleware = require('../middlewares/authMiddleware');
const {driverRoleMiddleware} = require('../middlewares/roleMiddleware');

router.all('/*', authMiddleware, driverRoleMiddleware);
router.get('/', getTrucks);
router.post('/', ifDriverFree, createTruck);
router.get('/:id', getTruck);
router.put('/:id', ifDriverFree, editTruck);
router.delete('/:id', ifDriverFree, deleteTruck);
router.post('/:id/assign', ifDriverFree, assignTruck);

module.exports = router;
